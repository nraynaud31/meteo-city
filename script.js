const api_get = "https://api.openweathermap.org/data/2.5/forecast?id=524901&appid=5eab3fa11a76d77184458db99d04388e";

let container = document.getElementById("container_div")
let content = document.getElementById("content")
let city_name = document.getElementById("city_name")
const p_max = document.createElement("p")
const img = document.getElementById("icon")
const h2_min = document.getElementById("min_title")
const h2_max = document.getElementById("max_title")
const h2_feels = document.getElementById("feels_title")
const p_min = document.createElement("p")
const p_feels = document.createElement("p")
const h2_wind = document.getElementById("wind_title")
const h2_speed = document.getElementById("speed_title")

// Title div section
const title = document.getElementById("title");

// Temp min
const temp_min = document.getElementById("temp_min");

// temp_max
const temp_max_div = document.getElementById("temp_max");

// Feels like
const feels_like_div = document.getElementById("feels_like");

// Speed
const speed_div = document.getElementById("speed");

// Direction
const direction_div = document.getElementById("direction");

// Humidity
const humidity_div = document.getElementById("humidity");

const p_wind_speed = document.createElement("p")
const p_wind_deg = document.createElement("p")
const h2_humidity = document.getElementById("humidity_title")
const p_humidity = document.createElement("p")

// On teste la connexion à l'API
fetch(api_get)
    .then(() => {
        console.log("L'API est bien fonctionnelle !");
    })
    .catch((error) => {
        console.log("Erreur: impossible d'interroger l'API !");
        console.error(error);
    })


// Press Enter detection
let send = document.getElementById('city-name');
send.addEventListener('keyup', (e) => {
    if (e.key === 'Enter') {
        showInfos();
    }
});

/**
 * On affiche toutes les infos (a également le rôle de fonction main)
 */
function showInfos() {
    let input_value = document.getElementById("city-name").value
    content.className = "d-flex mt-2 justify-content-center shadow-lg"
    const api_test = `https://api.openweathermap.org/geo/1.0/direct?q=${input_value},fr&limit=1&appid=5eab3fa11a76d77184458db99d04388e`;

    fetch(api_test).then((data) => {
        return data.json();
    })
        .then((datas) => {
            console.log(datas);
            // City Name
            title.appendChild(city_name);
            city_name.innerText = datas[0].name;
            const weather = `https://api.openweathermap.org/data/2.5/weather?lat=${datas[0].lat}&lon=${datas[0].lon}&units=metric&appid=5eab3fa11a76d77184458db99d04388e`;
            getWeatherInfos(weather)
        })
        .catch((error) => {
            console.log("Erreur: Impossible de récupérer les informations de "+ input_value + " ! Vérifiez le nom que vous avez entré!");
            console.error(error);
        })
}

/**
 * On récupère toutes les infos de Météo
 * @param weather Météo
 */
function getWeatherInfos(weather) {
    fetch(weather).then((data) => {
        return data.json();
    })
        .then((datas) => {
            console.log(datas);

            let icon_code = datas.weather[0].icon
            showImage(icon_code, datas)
            temp_min.appendChild(h2_min)
            getMinTemp(datas);
            temp_max_div.appendChild(h2_max)
            getMaxTemp(datas);
            feels_like_div.appendChild(h2_feels)
            getFeelsLike(datas)
            speed_div.appendChild(h2_speed)
            getWindSpeed(datas)
            direction_div.appendChild(h2_wind)
            getWindDirection(datas)
            humidity_div.appendChild(h2_humidity)
            getHumidity(datas)
        })
        .catch((error) => {
            console.log(error);
        })
}

/**
 * Température minimale
 * @param datas 
 */

function getMinTemp(datas) {
    h2_min.innerText = "Min"
    p_min.innerText = Math.round(datas.main.temp_min) + " °C"
    temp_min.appendChild(p_min)
}

/**
 * Température maximale
 * @param datas infos de Météo
 */
function getMaxTemp(datas) {
    h2_max.innerText = "Max"
    p_max.innerText = Math.round(datas.main.temp_max) + " °C"
    temp_max_div.appendChild(p_max)
}

/**
 * Ressenti (arrondi à l'unité)
 * @param datas infos de Météo
 */
function getFeelsLike(datas) {
    h2_feels.innerText = "Ressenties"
    p_feels.innerText = Math.round(datas.main.feels_like) + " °C"
    feels_like_div.appendChild(p_feels)
}

/**
 * Vitesse du vent
 * @param datas infos de Météo
 */
function getWindSpeed(datas) {
    h2_speed.innerText = "Vitesse";
    p_wind_speed.innerText = Math.round(datas.wind.speed * 3.6) + " km/h"
    speed_div.appendChild(p_wind_speed)
}

/**
 * Direction du vent (coordonnées + indication (N,S,E,O...))
 * @param datas infos de Météo
 */

function getWindDirection(datas) {
    h2_wind.innerText = "Vent";
    p_wind_deg.innerText = datas.wind.deg + " ° " + getWindDirectionName(datas)
    direction_div.appendChild(p_wind_deg)
}

/**
 * Le nom de la direction du vent (N, S, O, E...)
 * @param data Infos de météo
 * @returns Direction du vent (N,S,E,O...)
 */
function getWindDirectionName(data) {
    if (data.wind.deg > 330 && data.wind.deg <= 360) {
        return "(Est)"
    }

    if (data.wind.deg >= 290 && data.wind.deg < 330) {
        return "(SE)"
    }

    if (data.wind.deg > 250 && data.wind.deg < 290) {
        return "(Sud)"
    }
    if (data.wind.deg <= 250 && data.wind.deg > 200) {
        return "(SO)"
    }
    if (data.wind.deg < 200 && data.wind.deg > 160) {
        return "(Ouest)"
    }
    if (data.wind.deg <= 160 && data.wind.deg > 120) {
        return "(NO)"
    }
    if (data.wind.deg <= 120 && data.wind.deg > 80) {
        return "(Nord)"
    }
    if (data.wind.deg <= 80 && data.wind.deg > 40) {
        return "(NE)"
    }
    if (data.wind.deg >= 0 && data.wind.deg <= 40) {
        return "(Est)"
    }
}

/**
 * Taux d'humidité
 * @param datas infos de Météo
 */

function getHumidity(datas) {
    h2_humidity.innerText = "Humidité";
    p_humidity.innerText = datas.main.humidity + " %"
    humidity_div.appendChild(p_humidity)
}

/**
 * Image de Météo
 * @param icon_code code de l'icone 
 * @param datas infos de Météo
 */
function showImage(icon_code, datas) {
    const icon_url = "https://openweathermap.org/img/w/" + icon_code + ".png";
    img.src = icon_url
    img.alt = datas.weather[0].main
    title.appendChild(img)
}
